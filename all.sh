#!/bin/bash

./mkrootfs.sh i686 &
./mkrootfs.sh x86_64 &
./mkrootfs.sh armv7l &
./mkrootfs.sh aarch64 &
./mklive.sh -a i686 -i lz4 -s zstd -l es_ES.UTF8 -p "dialog lz4 grub NetworkManager dbus zstd" -S "NetworkManager dbus" &
./mklive.sh -a x86_64 -i lz4 -s zstd -l es_ES.UTF8 -p "dialog lz4 grub NetworkManager dbus zstd" -S "NetworkManager dbus" &